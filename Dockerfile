FROM myserver:withproxy
#linux 16.04
MAINTAINER Dimitris

WORKDIR /
### PORTAL #### 
WORKDIR portal
RUN git clone  http://172.18.22.160:8880/gitblit-1.8.0/r/oK/Portal/Backend.git && cd Backend && git checkout DEVELOP_BE 
WORKDIR Backend
RUN rm -f -r ./target/portal && rm -f -r ./target/portal.war
RUN mvn install -DskipTests
RUN mv ./target/portal.war /opt/tomcat/webapps/portal.war

WORKDIR /
### PORTAL FE ####
WORKDIR portal
RUN git clone  http://172.18.22.160:8880/gitblit-1.8.0/r/oK/Portal/Frontend.git  && cd Frontend && git checkout DEVELOP_FE
WORKDIR Frontend
RUN npm install
RUN ng build --prod
RUN mv ./dist/ /opt/tomcat/webapps/Frontend/

WORKDIR /
### DIAGNOSIS APP ####
WORKDIR microservices
RUN git clone http://172.18.22.160:8880/gitblit-1.8.0/r/Dropwizard/Microservices/mics-diagnosis-app.git && cd mics-diagnosis-app && git checkout DEVELOP_FE
WORKDIR mics-diagnosis-app
RUN npm install
RUN ng build --prod
RUN mv ./dist/ /opt/tomcat/webapps/mics-diagnosis-app/

WORKDIR /
### HOME SERVICE #### 
WORKDIR microservices
RUN git clone http://172.18.22.160:8880/gitblit-1.8.0/r/Dropwizard/Microservices/mics-home-service.git && cd mics-home-service && git checkout DEVELOP_BE 
WORKDIR mics-home-service
RUN rm -f -r ./target/mics-home-service && rm -f -r ./target/mics-home-service.war
RUN mvn install -DskipTests
RUN mv ./target/mics-home-service.war /opt/tomcat/webapps/mics-home-service.war

WORKDIR /
### PLGM FRONTEND ####
WORKDIR gridmeasures
RUN git clone http://172.18.22.160:8880/gitblit-1.8.0/r/oK/PlannedGridMeasures/Frontend.git   && cd Frontend && git checkout DEVELOP_FE
WORKDIR Frontend
RUN npm install
RUN ng build --prod
RUN mv ./dist/ /opt/tomcat/webapps/GridMeasuresFrontend/

WORKDIR /

CMD ["/opt/tomcat/bin/catalina.sh", "run"]

EXPOSE 8080


