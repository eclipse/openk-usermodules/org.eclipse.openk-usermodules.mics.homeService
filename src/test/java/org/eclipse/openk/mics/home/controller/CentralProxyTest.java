/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.mics.home.controller;

import org.eclipse.openk.mics.home.common.HttpStatusException;
import org.eclipse.openk.mics.home.communication.RestServiceWrapper;
import org.easymock.EasyMock;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

import static org.easymock.EasyMock.anyString;
import static org.junit.Assert.assertEquals;

public class CentralProxyTest {

    @Test
    public void testAll() throws HttpStatusException {
        RestServiceWrapper rsw = EasyMock.createMock(RestServiceWrapper.class);
        EasyMock.expect(rsw.performGetRequest(anyString())).andReturn("fine").anyTimes();
        EasyMock.replay(rsw);
        EasyMock.verify(rsw);
        CentralProxy cp = new CentralProxy("TEST_URL");

        Whitebox.setInternalState(cp, "restServiceWrapper", rsw);

        assertEquals( "fine", cp.getHealthState("/abs",
                "https", "123.456.789.0", "4711"));
        assertEquals( "fine", cp.getServerDistribution("klausDasCluster"));
    }
}
