/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.mics.home.common;


import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BackendConfigTest {
    @Test
    public void testConfig() {
        BackendConfig bc = BackendConfig.getInstance();
        assertEquals( bc.getMicsCentralURL(), "http://localhost:9010/mics/central" );
        assertEquals( bc.getMicsDistributionCluster(), "openK");
        assertEquals( bc.getMicsHealthStateExtraPath(), "healthcheck?pretty=true" );
        BackendConfig.setConfigFileName("backendConfigProduction.json");
        assertEquals( BackendConfig.getConfigFileName(), "backendConfigProduction.json");
        assertEquals( false, BackendConfig.getInstance().isMicsCentralIsHttps());
    }
}
