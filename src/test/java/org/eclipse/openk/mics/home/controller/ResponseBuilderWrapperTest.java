/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.mics.home.controller;


import org.apache.http.HttpStatus;
import org.eclipse.openk.mics.home.common.HttpStatusException;
import org.junit.Test;

import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;


public class ResponseBuilderWrapperTest {
    @Test
    public void testGetResponseBuilder() throws HttpStatusException {
        String json = "{ 'ret' : 'OK' }";
        Response.ResponseBuilder rb = ResponseBuilderWrapper.INSTANCE.getResponseBuilder( json );
        Response resp = rb.build();
        assertEquals(resp.getStatus(), HttpStatus.SC_OK );
    }

    @Test
    public void testBuildOkResponse() throws HttpStatusException {
        String json = "{ 'test' : 'Value' }";
        Response resp = ResponseBuilderWrapper.INSTANCE.buildOKResponse( json );
        assertEquals( resp.getStatus(), HttpStatus.SC_OK );

    }
}
