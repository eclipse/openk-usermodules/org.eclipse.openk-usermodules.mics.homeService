/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.mics.home.common;

import org.junit.Test;
import org.powermock.reflect.Whitebox;

public class InitBackendConfigTest {

    private InitBackendConfig initBackendConfig = new InitBackendConfig();

    @Test
    public void testSetConfigFiles() throws Exception {
        Whitebox.invokeMethod(initBackendConfig, "setConfigFiles", "DevLocal");
    }

    private void testGeneric(String environment ) throws Exception {
        Whitebox.invokeMethod(initBackendConfig, "setConfigFiles", environment);
    }

    @Test
    public void testCombinations() throws Exception {

        testGeneric("DevLocal");
        testGeneric("DevLocal");

        testGeneric("Docker");
        testGeneric("Docker");

        testGeneric("DevServer");
        testGeneric("DevServer");

        testGeneric("Custom");
        testGeneric("Custom");

        testGeneric(null);
        testGeneric(null);

        testGeneric("");
        testGeneric("");

}
}
