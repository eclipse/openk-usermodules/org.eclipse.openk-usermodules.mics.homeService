/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.mics.home.common.util;

import org.junit.Test;
import org.powermock.reflect.Whitebox;

import java.util.Date;

import static org.junit.Assert.assertEquals;

public class JsonGeneratorBaseTest extends ResourceLoaderBase {
    public static class TestPojo {
        private String name;
        private int number;
        private Date date;


        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getNumber() {
            return number;
        }

        public void setNumber(int number) {
            this.number = number;
        }

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }

    }

    @Test
    public void testInputOutput() throws Exception {
        Date now = new Date(System.currentTimeMillis());
        TestPojo pojo = new TestPojo();
        pojo.setDate(now);
        pojo.setName("Haferkamp");
        pojo.setNumber(6758);

        // call to reach sonar quality gate
        Whitebox.invokeConstructor(JsonGeneratorBase.class);

        String jsonString = JsonGeneratorBase.getGson().toJson(pojo);

        TestPojo pojoIncoming = JsonGeneratorBase.getGson().fromJson(jsonString, TestPojo.class);

        assertEquals(pojo.getDate(), pojoIncoming.getDate());
        assertEquals(pojo.getName(), pojoIncoming.getName());
        assertEquals(pojo.getNumber(), pojoIncoming.getNumber());
    }


}
