/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.mics.home.common;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class NotificationStatusTest {

	@Test
	public void createNotificationStatus() throws Exception {

        assertEquals(0, NotificationStatus.UNKNOWN.id);
        assertEquals(1, NotificationStatus.OPEN.id);
        assertEquals(2, NotificationStatus.INPROGRESS.id);
        assertEquals(3, NotificationStatus.FINISHED.id);
        assertEquals(4, NotificationStatus.CLOSED.id);

    }

}
