/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.mics.home.viewmodel;


import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class ServiceRequestEnvelopeTest {

    @Test
    public void testPojo() {
        final String payload = "Testme Accurate";
        ServiceRequestEnvelope env = new ServiceRequestEnvelope();
        env.setHttps(true);
        env.setMethod("POST");
        env.setUriFragment("Fraggel");
        env.setServiceName("Salve");

        ServiceRequestEnvelope.HttpHeader[] headers = new ServiceRequestEnvelope.HttpHeader[2];
        headers[0] = new ServiceRequestEnvelope.HttpHeader();
        headers[0].setAttribute("1stAttr");
        headers[0].setValue("1stValue");
        headers[1] = new ServiceRequestEnvelope.HttpHeader();
        headers[1].setAttribute("2ndAttr");
        headers[1].setValue("2ndValue");
        env.setHeaders(headers);

        assertEquals(true, env.isHttps());
        assertEquals("POST", env.getMethod());
        env.setPayload(payload);
        assertEquals(payload, env.getPayload());
        assertEquals("Fraggel", env.getUriFragment());
        assertEquals("Salve", env.getServiceName());
        assertEquals(2, env.getHeaders().length);
        assertEquals("2ndAttr", env.getHeaders()[1].getAttribute());
        assertEquals("1stValue", env.getHeaders()[0].getValue());
    }
}
