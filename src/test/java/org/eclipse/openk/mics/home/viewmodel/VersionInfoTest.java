/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.mics.home.viewmodel;


import static org.junit.Assert.assertTrue;

import org.eclipse.openk.mics.home.common.JsonGeneratorBase;
import org.eclipse.openk.mics.home.common.util.ResourceLoaderBase;
import org.junit.Test;

public class VersionInfoTest extends ResourceLoaderBase {
    // IMPORTANT TEST!!!
    // Make sure, our Interface produces a DEFINED Json!
    // Changes in the interface will HOPEFULLY crash here!!!

    @Test
    public void testStructureAgainstJson() {
        String json = super.loadStringFromResource("VersionInfo.json");
        VersionInfo vi = JsonGeneratorBase.getGson().fromJson(json, VersionInfo.class);

        assertTrue(vi.getBackendVersion().equals("0x.0x.xx"));
    }

    @Test
    public void testSetters() {
        VersionInfo vi = new VersionInfo();
        vi.setBackendVersion("222");
    }

}
