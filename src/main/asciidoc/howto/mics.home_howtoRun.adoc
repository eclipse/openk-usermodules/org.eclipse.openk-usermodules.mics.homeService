////
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
////
= openKonsequenz - How to run the module "mics.home@openK"
:Date: 2017-11-17
:Revision: 1
:icons:
:source-highlighter: highlightjs
:highlightjs-theme: solarized_dark

<<<

IMPORTANT: Please be sure that you have first *Portal (Auth n Auth)* installed and configured!

== Requirements
* Browser (Chrome or IE11 suggested)

== Prerequisites

* *To see this application running you have to run Portal application too.* The reason is the authentication, which happened in the Portal login phase.

* A developing and administrating software for databases


== How to run the Backend
To run the backend you need to have installed and configured Apache Tomcat (look at *mics.home_howtoBuild*).

=== Set up and start Apache Tomcat
Tomcat needs the Web Application Archive (war) file, which is produced by building the maven project, to run the application.

* Copy the *mics-home-app.war* file from the project file `/target` in the `<tomcat>/webapps` file. If there is a folder named *betriebstagebuch*, delete it before.
* Navigate to `C:\apache-tomcat-8.0.30\bin` and start Tomcat by clicking on *startup.bat*.
* Tomcat's default port is *8080*.

[source,text]
----
If 8080 port is already in use, you have to change tomcat's port as follows:
- Go to <tomcat>/conf folder
- Open server.xml file and search for "Connector port"
- Replace "8080" by your port number (for example 8181)
- Restart tomcat server (in the same folder which startup.bat is located, you can also find a shutdown.bat file).
----
{blank}

IMPORTANT: If you change the port number you have also adapt the port in the
frontend project: File "<Frontend-Path>/proxy.conf.json"

== How to run the Frontend
To run the frontend project you need to have installed and updated Node.js and npm Angular-CLI.

=== Compile the Frontend

To compile say Angular-CLI to start.

* Open a command line and navigate to the root folder of the frontend project
* Run the command

[source,command]
----
   $  npm start
----
{blank}

* Open a browser and type:

[source,http]
----
    http://localhost:4200
----
{blank}




