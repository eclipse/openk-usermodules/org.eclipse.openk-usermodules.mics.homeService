/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.mics.home.common;

import org.eclipse.openk.mics.home.common.util.ResourceLoaderBase;

public class BackendConfig {

  private static String configFileName = "backendConfigDevLocal.json";

  // properties of the configfile
  private String micsCentralURL;
  private String micsDistributionCluster;
  private String micsHealthStateExtraPath;
  private boolean micsCentralIsHttps;

  private static BackendConfig instance;

  private BackendConfig() {
  }

  public static synchronized BackendConfig getInstance() {
    if (instance == null) {
      String jsonConfig = loadJsonConfig();
      instance = JsonGeneratorBase.getGson().fromJson(jsonConfig, BackendConfig.class);
    }

    return instance;
  }

  private static String loadJsonConfig() {
    ResourceLoaderBase resourceLoaderBase = new ResourceLoaderBase();
    return resourceLoaderBase.loadStringFromResource(configFileName);
  }

  public String getMicsCentralURL() {
    return micsCentralURL;
  }

  public String getMicsDistributionCluster() {
    return micsDistributionCluster;
  }

  public String getMicsHealthStateExtraPath() {
    return micsHealthStateExtraPath;
  }

  public boolean isMicsCentralIsHttps() {
    return micsCentralIsHttps;
  }

  public static String getConfigFileName() {
    return configFileName;
  }

  public static void setConfigFileName(String configFileName) {
    BackendConfig.configFileName = configFileName;
  }
}


