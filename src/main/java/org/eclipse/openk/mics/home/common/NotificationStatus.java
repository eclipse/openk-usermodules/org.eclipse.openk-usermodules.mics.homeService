/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.mics.home.common;

public enum NotificationStatus {

	UNKNOWN(0, "unknown"),
	OPEN(1, "open"),
	INPROGRESS(2, "inprogress"),
	FINISHED(3, "finished"),
	CLOSED(4, "closed");

    public final int id;
	public final String statusName;

	NotificationStatus(int id, String name) {
        this.id = id;
        this.statusName = name;
	}
}
