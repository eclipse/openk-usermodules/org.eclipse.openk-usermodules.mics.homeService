/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.mics.home.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.eclipse.openk.mics.home.common.JsonGeneratorBase;
import org.eclipse.openk.mics.home.communication.RestServiceWrapper;
import org.eclipse.openk.mics.home.controller.BackendController;
import org.eclipse.openk.mics.home.controller.CentralProxy;
import org.eclipse.openk.mics.home.controller.ResponseBuilderWrapper;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.eclipse.openk.mics.home.common.BackendConfig;
import org.eclipse.openk.mics.home.common.HttpStatusException;
import org.eclipse.openk.mics.home.viewmodel.VersionInfo;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Api(value = "/mics/home")
@ApiResponses( value ={
		@ApiResponse(code = 200, message = "OK"),
		@ApiResponse(code = 400, message = "Bad Request"),
		@ApiResponse(code = 401, message = "Unauthorized"),
		@ApiResponse(code = 404, message = "Not found"),
		@ApiResponse(code = 423, message = "Locked"),
		@ApiResponse(code = 500, message = "Internal Server Error") } )
@Path("/mics/home")
public class BackendRestService {
	private static final Logger logger = Logger.getLogger(BackendRestService.class.getName());

	@ApiOperation(value = "Version Information", notes = "This services displays the version infos of this backend and the connected database.")
	@ApiResponses( value ={ @ApiResponse(code = 200, message = "OK", response = VersionInfo.class,reference = "#/definitions/VersionInfo") } )
	@GET
	@Path("/versionInfo/")
	@Produces("application/json")
	public Response getVersionInfo() {
		String vstring = JsonGeneratorBase.getGson().toJson(
				new BackendController().getVersionInfo() );
		try {
			return ResponseBuilderWrapper.INSTANCE.buildOKResponse(vstring);
		} catch (HttpStatusException e) {
			return responseFromException(e);
		}
	}

	@ApiOperation(value = "Server Distribution", notes = "This services returns the available distributions for a the clusters.")
	@ApiResponses( value ={ @ApiResponse(code = 200, message = "OK") } )
	@GET
	@Path("/myServerDistribution/")
	@Produces("application/json")
	public Response getMyServerDistribution() {
		logger.debug("myServerDistribution called!");
		try {
			BackendConfig bc = BackendConfig.getInstance();
			CentralProxy central = new CentralProxy(bc.getMicsCentralURL());
			logger.debug("Forwarding to central on:"+bc.getMicsCentralURL());

			return ResponseBuilderWrapper.INSTANCE.buildOKResponse(
					central.getServerDistribution(bc.getMicsDistributionCluster()));
		}
		catch (Exception e) {
			logger.error("Error in myServerDistribution", e);
			return responseFromException(e);
		}
	}

	@ApiOperation(value = "Health state check", notes = "This service checks if the components of a service are healthy")
	@ApiResponses( value ={ @ApiResponse(code = 200, message = "OK") } )
	@GET
	@Path("/healthState/{protocol}/{host}/{port}")
	@Produces("application/json")
	public Response getHealthState(@ApiParam(name = "protocol", value = "The name of the protocol is used(http/https)")
                                   @PathParam("protocol") String protocol,
                                   @ApiParam(name = "host", value = "The name of the host")
                                   @PathParam("host") String host ,
                                   @ApiParam(name = "port",  value = "The number of the port")
                                   @PathParam("port") String port ) {
		logger.debug("healthState called!");
		try {
			BackendConfig bc = BackendConfig.getInstance();
			CentralProxy central = new CentralProxy(bc.getMicsCentralURL());
			return ResponseBuilderWrapper.INSTANCE.buildOKResponse(
					central.getHealthState(bc.getMicsHealthStateExtraPath(), protocol, host, port));
		}
		catch (Exception e) {
			logger.error("Exception in healthState!", e);
			return responseFromException(e);
		}
	}

	@ApiOperation(value = "Dispatcher", notes = "")
	@ApiResponses( value ={ @ApiResponse(code = 200, message = "OK") } )
	@POST
	@Path("/dispatch/")
	public Response dispatch( String envelope ) {
		logger.debug("dispatch called!");
		try {
			BackendConfig bc = BackendConfig.getInstance();
			RestServiceWrapper restServiceWrapper = new RestServiceWrapper(bc.getMicsCentralURL(), bc.isMicsCentralIsHttps());
			return restServiceWrapper.performPostRequestWithResponse(createDispatchURL(), envelope);
		}
		catch( Exception e) {
			logger.error("Exception in dispatch!", e);
			return responseFromException(e);

		}
	}

	private String createDispatchURL() {
		BackendConfig bc = BackendConfig.getInstance();
		return "dispatch/"+bc.getMicsDistributionCluster();
	}


	private Response responseFromException(Exception e) {
		int errcode;

		if (e instanceof HttpStatusException) {
			logger.error("Caught BackendException", e);
			errcode = ((HttpStatusException) e).getHttpStatus();
			return Response.status(errcode).build();
		} else {
			logger.error("Unexpected exception", e);
			return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).build();
		}
	}
}
