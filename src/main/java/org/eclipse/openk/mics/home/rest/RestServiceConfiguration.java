/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.mics.home.rest;


import java.util.HashSet;
import java.util.Set;

public class RestServiceConfiguration extends javax.ws.rs.core.Application {
    public RestServiceConfiguration() {
        // Standard Contstructor needed
    }

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> restServicesClasses = new HashSet<>();
        restServicesClasses.add(BackendRestService.class);
        return restServicesClasses;
    }
}
