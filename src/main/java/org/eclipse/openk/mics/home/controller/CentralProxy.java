/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.mics.home.controller;

import org.eclipse.openk.mics.home.common.HttpStatusException;
import org.eclipse.openk.mics.home.communication.RestServiceWrapper;

public class CentralProxy {
    private final String url;
    private final RestServiceWrapper restServiceWrapper;

    public CentralProxy( String url ) {
        this.url = url;
        this.restServiceWrapper = new RestServiceWrapper( url, false );
    }

    public String getServerDistribution( String clusterName ) throws HttpStatusException {
        return restServiceWrapper.performGetRequest(url + "/serviceDistribution/" + clusterName );
    }

    public String getHealthState( String healthStateExtraPath, String protocol, String host, String port ) throws HttpStatusException {
        return restServiceWrapper.performGetRequest(protocol + "://" + host + ":" + port + "/" + healthStateExtraPath );
    }

}
