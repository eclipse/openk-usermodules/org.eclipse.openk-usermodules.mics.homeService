/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.mics.home.common;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

public class InitBackendConfig extends HttpServlet {

    private static final long serialVersionUID = -7882117179312471533L;

    private static final Logger LOGGER = Logger.getLogger(InitBackendConfig.class.getName());

    @Override
    public void init() throws ServletException {
        String environment = getServletContext().getInitParameter("OK_MICS_HOME_ENVIRONMENT");
        setConfigFiles(environment);
    }

    private void setConfigFiles(String environment ) {
        String env = (environment == null ? "Production": environment);

        String backendConfigFile;

        switch (env){
            case "DevLocal":
                backendConfigFile="backendConfigDevLocal.json";
                break;
            case "Docker":
                backendConfigFile="backendConfigDocker.json";
                break;
            case "DevServer":
                backendConfigFile="backendConfigDevServer.json";
                break;
            case "Custom":
                backendConfigFile="backendConfigCustom.json";
                break;
            default:
                backendConfigFile="backendConfigProduction.json";
        }

        BackendConfig.setConfigFileName(backendConfigFile);
        LOGGER.info("Portal backendendenviroment is: " +environment+ ". Setting backendConfig accordingly to: "+ backendConfigFile);
    }
}
