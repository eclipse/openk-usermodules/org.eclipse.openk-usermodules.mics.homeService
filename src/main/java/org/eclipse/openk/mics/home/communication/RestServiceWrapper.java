/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.mics.home.communication;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.eclipse.openk.mics.home.common.HttpStatusException;
import org.apache.log4j.Logger;
import org.eclipse.openk.mics.home.common.Globals;


import javax.ws.rs.core.Response;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class RestServiceWrapper {
    private static final Logger logger = Logger.getLogger(RestServiceWrapper.class.getName());
    private String baseURL;
    private boolean useHttps;

    public RestServiceWrapper(String baseURL, boolean https) {
        this.baseURL = baseURL;
        this.useHttps = https;
    }

    public String performGetRequest(String restFunctionWithParams) throws HttpStatusException {
        String completeRequest = restFunctionWithParams;
        logger.debug("CompleteUrl: " + completeRequest);
        // create HTTP Client
        try (CloseableHttpClient httpClient = createHttpsClient() ) {
            // create new Request with given URL
            HttpGet getRequest = new HttpGet(completeRequest);
            getRequest.addHeader("accept", Globals.HEADER_JSON_UTF8);

            HttpResponse response;
            // Execute request an catch response
            response = httpClient.execute(getRequest);

            return createJson(response);
        } catch (IOException e) {
            String errtext = "Communication to <" + completeRequest + "> failed!";
            logger.warn(errtext, e);
            throw new HttpStatusException(HttpStatus.SC_SERVICE_UNAVAILABLE);
        }

    }

	public Response performPostRequestWithResponse(String restFunctionWithParams, String data) throws HttpStatusException {
		String completeRequest = baseURL + "/" + restFunctionWithParams;

		// create HTTP Client
		CloseableHttpClient httpClient = createHttpsClient();

		// create new Post Request with given URL
		HttpPost postRequest = new HttpPost(completeRequest);

		// add additional header to getRequest which accepts application/JSON data
		postRequest.addHeader("accept", Globals.HEADER_JSON_UTF8);
		postRequest.addHeader("Content-Type", Globals.HEADER_JSON_UTF8);

		postRequest.setEntity(new StringEntity(data, StandardCharsets.UTF_8));

		HttpResponse response;
		// Execute request an catch response
		try {
			response = httpClient.execute(postRequest);
		} catch (IOException e) {
			String errtext = "Communication to <" + completeRequest + "> failed!";
			logger.warn(errtext, e);
            throw new HttpStatusException(HttpStatus.SC_SERVICE_UNAVAILABLE);
		}
        String ent = createJson(response);

        Response.ResponseBuilder rb = Response.status(response.getStatusLine().getStatusCode());
        if (ent != null) {
            rb.entity(ent);
        }
        return rb.build();
	}

    private CloseableHttpClient createHttpsClient() throws HttpStatusException {
        if (useHttps) {
            try {
                SSLContextBuilder builder = new SSLContextBuilder();
                builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
                SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(builder.build());

                return HttpClients.custom().setSSLSocketFactory(sslsf).build();
            } catch (Exception e) {
                logger.error(e);
                throw new HttpStatusException(HttpStatus.SC_INTERNAL_SERVER_ERROR);
            }
        } else {
            return HttpClientBuilder.create().build();
        }
    }
    
	private String createJson(HttpResponse response) throws HttpStatusException {
		String retJson;
		try {
			retJson = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
		} catch (IOException e) {
			logger.error(e);
			throw new HttpStatusException(HttpStatus.SC_INTERNAL_SERVER_ERROR);
		}

		return retJson;
	}

}
