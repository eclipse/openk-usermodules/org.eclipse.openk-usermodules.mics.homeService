/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.mics.home.viewmodel;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ServiceRequestEnvelope {
    public static class HttpHeader {
        private String attribute;
        private String value;

        public String getAttribute() {
            return attribute;
        }

        public void setAttribute(String attribute) {
            this.attribute = attribute;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    private String serviceName;
    private boolean isHttps;
    private String method;
    private String uriFragment;
    private String payload;
    private HttpHeader[] headers;

    @JsonProperty
    public String getServiceName() {
        return serviceName;
    }

    @JsonProperty
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    @JsonProperty
    public boolean isHttps() {
        return isHttps;
    }

    @JsonProperty
    public void setHttps(boolean https) {
        isHttps = https;
    }

    @JsonProperty
    public String getMethod() {
        return method;
    }

    @JsonProperty
    public void setMethod(String method) {
        this.method = method;
    }

    @JsonProperty
    public String getUriFragment() {
        return uriFragment;
    }

    @JsonProperty
    public void setUriFragment(String uriFragment) {
        this.uriFragment = uriFragment;
    }

    @JsonProperty
    public String getPayload() {
        return payload;
    }

    @JsonProperty
    public void setPayload(String payload) {
        this.payload = payload;
    }

    @JsonProperty
    public HttpHeader[] getHeaders() {
        return headers;
    }

    @JsonProperty
    public void setHeaders(HttpHeader[] headers) {
        this.headers = headers;
    }
}
