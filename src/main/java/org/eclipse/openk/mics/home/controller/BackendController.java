/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.mics.home.controller;

import org.apache.log4j.Logger;
import org.eclipse.openk.mics.home.viewmodel.VersionInfo;

public class BackendController {
	
	private static final Logger LOGGER = Logger.getLogger(BackendController.class.getName());

	public VersionInfo getVersionInfo() {
		LOGGER.info("getVersionInfo is called");

		try {
			return getVersionInfoImpl(getClass().getPackage().getImplementationVersion());
		} finally {
			LOGGER.info("getVersionInfo is finished");
		}
	}

	private VersionInfo getVersionInfoImpl(String pomVersion) {
		VersionInfo vi = new VersionInfo();
		vi.setBackendVersion(pomVersion);

		return vi;
	}
}
